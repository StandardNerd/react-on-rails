import React from "react";

export default class ButtonComponent extends React.Component {

  render() {
    return (
      <div>
        <button className="color-button">Red</button>
        <button className="color-button">Blue</button>
      </div>
    );
  }
}