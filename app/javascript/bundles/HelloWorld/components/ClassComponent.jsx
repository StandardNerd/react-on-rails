import React from "react";
import Glamorous, { Div } from "glamorous";
import "./counter.css";

// use Glamorous to style this component
const Heading = Glamorous.h1({
  fontSize: "2.4em",
  marginTop: 10,
  color: "#CC3A4B"
});

const Paragraph = Glamorous.p({
  fontSize: "1em",
  padding: 10,
  color: "lightgrey"
});

// React Einführung Teil 3:
// https://www.heise.de/developer/artikel/Einfuehrung-in-React-Folge-2-Ein-tieferer-Einblick-3852368.html

/*
React Concept:

Funktionen, die Daten übergeben bekommen und dann UI ausgeben

f(data) => VIEW

*/


class InputProcessing extends React.Component {

  handleInputProcessing = () => {
    console.log(this.input.value)
  }

  render() {
    return (
      <div>
        <label htmlFor="">Name: </label>
        <input type="text" name="name" ref={input => this.input = input} />
        <button onClick={this.handleInputProcessing}>Eingabe verarbeiten</button>
      </div>
    );
  }
}

class Probability {
  constructor(probability) {
    if (probability < 0) {
      throw new Error("Invalid probability");
    }

    if (probability > 1) {
      throw new Error("Invalid probability");
    }

    this.probability = probability;
  }

  equals(other) {
    const epsilon = 0.1;
    return Math.abs(this.propability - other.probability) < epsilon;
  }

  inverse() {
    // this.probability = 1 - this.probability;
    // return this.probability;
    return new Probability(1 - this.probability);
  }

  combined(other) {
    // this.probability = this.probability * other.probability;
    // return this.probability;
    return new Probability(this.probability * other.probability);
  }
}

// const p1 = new Probability(0.3);
// console.log(p1.probability);
// console.log(p1.equals(0.29));
// console.log(p1.inverse().probability);
// console.log(p1.probability);

// Anstelle von Objekt-Referenzen besser Objekt-Duplikate erstellen, damit Veränderungen am Duplikat das urspr. Objekt nicht ändern
const obj1 = { counter: 1 };
const obj2 = obj1;
obj2.counter = 2;
obj1 === obj2; // true ,da obj1.counter ebenfalls auf 2 gesetzt wurde

const obj3 = { ...obj1, counter: 3 };
obj1 === obj3; // false ,da obj1.counter noch den urspr. Wert hat und nicht auf 3 gesetzt wurde

// React Einführung Teil 2:
// https://www.heise.de/developer/artikel/Einfuehrung-in-React-Folge-2-Ein-tieferer-Einblick-3852368.html

function formatAsBinary(dec) {
  return dec.toString(2);
}

class Count extends React.PureComponent {
  // shouldComponentUpdate(nextProps, nextState) {
  //   return this.props.quantity !== nextProps.quantity;
  // }

  render() {
    return <Heading>{formatAsBinary(this.props.quantity.value)}</Heading>;
  }
}

// Für Interaktion ClassComponent - im Ggs. zu FunctionalComponent -
class ClassComponent extends React.Component {
  constructor(props) {
    super(props); // super(), in most class based languages means call the parent's constructor.
    // So it would be calling React.Component's constructor.
    this.state = { counter: { value: 0 } }; // Objekt mit Attribut value = 0
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    // componentDidMount() is invoked immediately after a component is mounted.
    // this.setState({ counter: 5 });
    // console.log(this.state.counter); // ergibt 0 anstelle von 5, da Aufrufe von setState asynchron erfolgen
    this.intervalId = setInterval(() => {
      if (this.state.counter.value > 9) {
        clearInterval(this.intervalId);
      } else {
        this.setState((prevState, props) => {
          const newCounter = { ...prevState.counter };
          newCounter.value = newCounter.value + 1;
          return { counter: newCounter };
        });
      }
    }, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.intervalId);
  }

  // componentDidMount() { // componentDidMount() is invoked immediately after a component is mounted.
  //   this.setState({ counter: 5 });
  //   console.log(this.state.counter); // ergibt 0 anstelle von 5, da Aufrufe von setState asynchron erfolgen
  //   this.intervalId = setInterval(this.counter, 1000);
  // }

  // counter() {
  //   this.setState((prevState, props) => ({counter: prevState.counter + 1}));
  // }

  handleClick() {
    console.log(this);
    this.setState({ counter: { value: 0 } });
  }

  render() {
    console.log("render(...)", this.state);
    return (
      <Div textAlign="right">
        <Heading>{this.props.name}</Heading>
        {/* props als Eigenschaft des Objektes daher: this.props.name */}
        <InputProcessing />
        <hr/>
        <Count quantity={this.state.counter} />
        <button onClick={this.handleClick}>reset</button>
        <Paragraph>styled H1 from Glamour</Paragraph>
      </Div>
    );
  }
}

export default ClassComponent;