import React from 'react';

const Greeting = (props, _railsContext) => (
  <div>Hello, I'm {props.text} </div>
)

export default Greeting;