import React from 'react';

// Einführung in React Teil 1

// React ist auf den Prinzipien der Functional Programming aufgebaut
// Pure Functions:
// - Aufruf mit gleichen Parametern liefert immer gleiches Ergebnis (Idempotenz)
// - Seiteneffektfreiheit
// - Unabhängigkeit von extern veränderlichem State 
// Beispiel einer Pure Function:
// function pure(props) { // {x: 4, y:5}
//   return props.x + props.y;
// }
// 
// Beispiel einer Impure Function:
// let z = 0;
// function impure(props) {
//   z++; // bei jedem Funktionsaufruf inkrementiert -> erhöht z ausserhalb des Function Scopes
//   props.y = 42; // Objekt werden Funktionen als Referenz übergeben, nachhaltig das Ursprungsobjekt y wird verändert
//   alert(z); // Seiteneffekt
//   window.foo = 'bar'; // Seiteneffekt: erzeugen Variable im globalen Kontext
//   return props.x + props.y + z; // nicht Idempotent, da z sich bei jedem Aufruf verändert und ein anderes Ergebnis zurückgibt
// }
// Verletzung mehrer Bedingungen für Pure Functions:
// - Keine Idempotenz
// - Keine Seiteneffektfreiheit
// - Abhängigkeit von extern veränderlichem State


// FunctionalComponent ist nur für reine UI-Repräsentation zuständig, read-only props werden angezeigt:
const FunctionalComponent = (props) => (
  <div>Hello, {props.name} </div>
)

export default FunctionalComponent;