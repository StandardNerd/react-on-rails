import React from "react";

const Ahoj = props => <div>Message from Ahoj-Component: {props.msg}</div>;

const FunctionalComponent = props => <div>{props.name}</div>;

export default class Goodbye extends React.Component {
  constructor() {
    super();
    this.state = {
      greeting: "Cau World!",
      parentMessage: "Cau Alice!"
    };

    setTimeout(() => {
      this.setState({ greeting: "ahoj world updated!" });
    }, 3000);
  }

  render() {
    return (
      <div className="greeting">
        <h4>{this.state.greeting}</h4>
        <FunctionalComponent name="Ahoj" />
        <FunctionalComponent name={this.state.parentMessage} />
      </div>
    );
  }
}