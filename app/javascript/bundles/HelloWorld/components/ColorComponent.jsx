import React from "react";

export default class ColorComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            color: "darkgrey"
        }
    }
    
    render() {
        return (
            <div className="color-container" style={{height: 40, backgroundColor: this.state.color}} />
        );
    }
}