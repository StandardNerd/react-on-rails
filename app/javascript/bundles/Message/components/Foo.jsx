import React from "react";
import sendToErrorReporting from './sendToErrorReporting';

var people = [
  {
    name: "Anderson Turner",
    avatar:
      "https://s3.amazonaws.com/uifaces/faces/twitter/craigrcoles/128.jpg",
    id: 0
  },
  {
    name: "Freddy Jones",
    avatar:
      "https://s3.amazonaws.com/uifaces/faces/twitter/ivanfilipovbg/128.jpg",
    id: 1
  },
  {
    name: "Angus Baumbach",
    avatar:
      "https://s3.amazonaws.com/uifaces/faces/twitter/abovefunction/128.jpg",
    id: 2
  },
  {
    name: "Sister Altenwerth",
    avatar: "https://s3.amazonaws.com/uifaces/faces/twitter/ryandownie/128.jpg",
    id: 3
  }
];

class MyErrorBoundary extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hasError: false
        };
    }

    componentDidCatch(error, info) {
        this.setState(state => ({ ...state, hasError: true }));
        sendToErrorReporting(error, info);
    }

    render() {
        if (this.state.hasError) {
            return <div>Sorry, something went wrong.</div>;
        } else {
            return this.props.children;
        }
    }
}

class Card extends React.Component {
  render() {
    return (
      <div className="person">
        <h2>{this.props.name}</h2>
        <img src={this.props.avatar} alt="" />
        <div />
        <button onClick={this.props.onClick}>Delete Me</button>
      </div>
    );
  }
}

class Foo extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
        people
    }
  }

  deletePerson(person) {
    this.state.people.splice(this.state.people.indexOf(person), 1);
    this.setState({ people: this.state.people });
  }

  render() {
    var that = this;
    if (this.state.hasError) {
      return <div>Sorry, something went wrong</div>;
    } else {
      return (
        <div>
            <MyErrorBoundary>
          {this.state.people.map(function(person) {
            return (
              <Card
                key={person.id}
                onClick={that.deletePerson.bind(null, person)}
                name={person.name}
                avatar={person.avatar}
              />
            );
          }, this)}
          </MyErrorBoundary>
        </div>
      );
    }
  }
}

export default Foo