import ReactOnRails from 'react-on-rails';

import HelloWorld from '../bundles/HelloWorld/components/HelloWorld';
import ButtonComponent from '../bundles/HelloWorld/components/ButtonComponent';
import ColorComponent from '../bundles/HelloWorld/components/ColorComponent';
import Greeting from '../bundles/HelloWorld/components/Greeting';
import Message from '../bundles/Message/components/Message';
import Foo from '../bundles/Message/components/Foo';
import FunctionalComponent from '../bundles/HelloWorld/components/FunctionalComponent';
import ClassComponent from '../bundles/HelloWorld/components/ClassComponent';

// This is how react_on_rails can see the HelloWorld in the browser.
ReactOnRails.register({
  FunctionalComponent,
  ClassComponent,
  ButtonComponent,
  ColorComponent,
  Message,
  Greeting
});
